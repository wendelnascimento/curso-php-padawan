<?php

// Calcular e exibir a área de um quadrado a partir do valor de sua diagonal que será digitado.
$diagonal;
$area;

echo "\nDigite o valor da diagonal: ";
$diagonal = trim(fgets(STDIN));

$area=$diagonal*$diagonal/2;

echo "\nA área é: $area";