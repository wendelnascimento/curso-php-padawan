<?php
// Entrar via teclado com o valor de cinco produtos. Após as entradas, digitar um valor referente ao
// pagamento da somatória destes valores. Calcular e exibir o troco que deverá ser devolvido.

$produto1;
$produto2;
$produto3;
$produto4;
$produto5;
$dinheiro;
$troco;

echo "\nDigite o valor do produto 1: ";
$produto1 = trim(fgets(STDIN));

echo "\nDigite o valor do produto 2: ";
$produto2 = trim(fgets(STDIN));

echo "\nDigite o valor do produto 3: ";
$produto3 = trim(fgets(STDIN));

echo "\nDigite o valor do produto 4: ";
$produto4 = trim(fgets(STDIN));

echo "\nDigite o valor do produto 5: ";
$produto5 = trim(fgets(STDIN));

echo "\nDigite o valor do pagamento: ";
$dinheiro = trim(fgets(STDIN));

$troco = $dinheiro-($produto1+$produto2+$produto3+$produto4+$produto5);


echo "\nO valor do troco é: R$$troco";