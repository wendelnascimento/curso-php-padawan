<?php

// aritmeticos
echo "Operadores aritimeticos:\n\n";

$soma           = 2 + 2;
$subtracao      = 10 - 3;
$multiplicacao  = 2 * 2;
$divisao        = 10 /2;
$modulo         = 10 % 3;

echo "\$soma          = $soma\n";
echo "\$subtracao     = $subtracao\n";
echo "\$multiplicacao = $multiplicacao\n";
echo "\$divisao       = $divisao\n";
echo "\$modulo        = $modulo";


echo "\n\n\nOperadores atribuicao:\n\n";

// atribuicao ' = '

$minhaString = 'Curso PHP';
$minhaString .= ' Padawan';     // Curso PHP Padawan

$numeroInteiro = 10;    // 10
$numeroInteiro += 10;   // 20
$numeroInteiro -= 10;   // 10

$numeroInteiro *= 3;    // 30
$numeroInteiro /= 3;    // 10;

$numeroInteiro++;       // 11
$numeroInteiro--;       // 10

$numeroInteiro2 = ++$numeroInteiro + 1  // $numeroInteiro2 = 12 | $numeroInteiro = 11
$numeroInteiro2 = $numeroInteiro++      // $numeroInteiro2 = 11 | $numeroInteiro = 12

echo "\n\n\nOperadores de comparacao:\n\n";

$a = 1;
$b = 1;

echo $a == $b; // true, pois tem o mesmo valor
echo $a === $b; // true, pois tem o mesmo valor e mesmo tipo

$b = "1"; // $b, agora é uma string

echo $a == $b; // true, pois tem o mesmo valor
echo $a === $b; // false, pois tem o mesmo valor porém tipo diferente

$b = 2;

echo $a != $b; // true, pois tem valores distintos
echo $a === $b; // false, mesmo tipo mas valores diferentes

$b = "2";

echo $a != $b; // true, pois tem valores distintos
echo $a === $b; // true, pois tem tipos e valores diferentes

$b = 2;

$a > $b; // false
$a < $b; // true

$a >= $b; // false
$a <= $b; // true

echo "\n\n\nOperadores logicos:\n\n";

$verdadeiro = true;
$falso = false;

echo $verdadeiro and $falso; // false
echo $verdadeiro and $verdadeiro; // true

echo $verdadeiro && $falso; // false
echo $verdadeiro && $verdadeiro; // true

echo $verdadeiro or $falso; // true
echo $verdadeiro || $falso; // true

echo !$verdadeiro; // false
echo !$falso; // true



