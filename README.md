# Curso PHP para Padawans

Curso básico de PHP voltado para backends Padawan

Será ministrado 3 vezes na semana, com aulas de 60 minutos e a cada fim de será passada uma lista de exercícios para ser entregue na aula seguinte. Para fazer os exercícios os participantes deverão realizar um ``fork`` deste repositório e submeter um ``pull request`` para que suas soluções possam ser avaliadas e discutidas em aula.

**Autores:** Bruno Gomes de Souza e Leonam Pereira Dias

**Início das aulas:** 13/05/2015 às 10 horas.

### Premissas:

Usaremos os seguintes tópicos para fortalecer o aprendizado:

* Fluxograma
* Git

## Conteúdo do curso:

O Curso será dividido da seguinte forma:

### PHP

* Overview sobre a linguagem
* Operadores
* Tipos de variáveis
* Estruturas de condição (if , else switch/case)
* Estruturas de repetição
* Funções básicas do PHP
* Identificando e trabalhando com os erros (Boas práticas)
* Array
	- Indexado, multi dimensional, associativo
	- Uso de índices e operações em arrays
* PHP para Web
	- Formulários
	- Get e Post
* Conceitos de Orientação a Objeto
	- Objeto
	- Classes
		- Propriedades
		- Métodos
	- Herança
	- Classes abstratas
	- Interface
	- Polimorfismo
	- Jeito certo de programar OO

### MySQL
	
* Sintaxe SQL
	- Operações CRUD
	- Joins
	- Evitando Injection
	- Bibliotecas: mysqli, PDO
	- Tipos de dados: int, bigint, varchar,text

### Drupal

* Instalação
* Módulos
* Temas
* Features
* Tipos de Conteúdo
* Administração de Conteudo